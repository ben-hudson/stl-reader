#stl-reader

STL Reader parses [binary STL files](http://en.wikipedia.org/wiki/STL_%28file_format%29) and outputs a [JSON representation](http://www.json.org/) of their structure.

###How do I save my 3D model as a binary STL file?

Most CAD software offers the option to save to an STL format. Because ASCII STL files often become very large, your software will probably save to a binary format by default.

To check if your STL file is in the right format, open it with a simple text editor. If your file only contains numbers, then you have a binary STL file. If your file starts with the words `solid <name of your model>` it is an ASCII STL file.

###How do I use this script?

In order to use this script, you will need to install [Python 2.x](https://www.python.org/downloads/). Once you have installed Python, you can run this script from the command line.

Usage:

    python stlreader.py input [-h] [-o]

Example:

    python stlreader.py example/cube.stl -o example/cube.json
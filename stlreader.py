import argparse, struct, json

parser = argparse.ArgumentParser()
parser.add_argument('input', type=str, help='path to input file')
parser.add_argument('-o', '--output', type=str, help='path to output file')
args = parser.parse_args()

fin = open(args.input, 'rb')

name = fin.read(80).split()[1]
num_triangles = struct.unpack('I', fin.read(4))[0]
triangles = []

for i in range(num_triangles):
    normal = struct.unpack('fff', fin.read(12))
    vertices = [struct.unpack('fff', fin.read(12)), struct.unpack('fff', fin.read(12)), struct.unpack('fff', fin.read(12))]
    triangles.append({'normal': normal, 'vertices': vertices})
    fin.read(2)

if args.output != None:
	fout = open(args.output, 'w')
	fout.write(json.dumps({'name': name, 'num_triangles': num_triangles, 'triangles': triangles}, sort_keys=True, indent=2))
	print 'data written to', args.output
else:
	print json.dumps({'name': name, 'num_triangles': num_triangles, 'triangles': triangles}, sort_keys=True, indent=2)